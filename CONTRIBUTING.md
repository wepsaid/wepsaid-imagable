## Coding Guidelines

* Pull requests for the latest major release MUST be sent to the master branch.
* To preserve the quality of the package, only well tested code will by merged.