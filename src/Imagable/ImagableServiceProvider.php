<?php namespace Wepsaid\Imagable;

use Illuminate\Support\ServiceProvider;

class ImagableServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/imagable.php' => config_path('imagable.php'),
        ]);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->bind('imagable', function () {
            return new Imagable();
        });
		
        $this->mergeConfigFrom(
            __DIR__.'/../config/imagable.php', 'imagable'
        );
    }
}
