<?php

namespace Wepsaid\Imagable;

use Illuminate\Support\Facades\Facade;

class ImagableFacade extends Facade
{
    /**
     * Get the registered name of the component.
     */
    protected static function getFacadeAccessor() : string
    {
        return 'imagable';
    }
}
