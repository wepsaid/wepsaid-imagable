<?php namespace Wepsaid\Imagable;

use GlideImage;
use Cache;
use Storage;
use Exception;
use Illuminate\Support\Collection;

class Imagable
{
	protected $path;
	protected $paths;
	protected $fileInfo;
	
    public static function path($path) : Imagable
    {
		if(is_array($path)) {
			return (new static())->setPaths($path);
		}elseif($path instanceof Collection) {
	        return (new static())->setPaths($path->toArray());
		}else{
			return (new static())->setPath((string) $path);
		}
    }
	
	public function setPath($path)
	{
		$this->paths = null;
		$this->path = $this->cleanPath($path);
		
		return $this;
	}
	
	public function setPaths(array $paths)
	{
		$this->path = null;
		
		$paths = array_map(function($item) { return $this->cleanPath($item); }, $paths);
		
		$this->paths = $paths;
		
		return $this;
	}
	
	public function cleanPath($path = null) 
	{
		return urldecode(trim($path, '/'));
	}
	
	public function getCacheInfo()
	{
		if(Cache::tags(config('imagable.cache_tag'))->has(config('imagable.cache_key'))) {
			$cachedImageThumbs = Cache::tags(config('imagable.cache_tag'))->get(config('imagable.cache_key'));
		}else{
			$cachedImageThumbs = [];
		}
		
		return $cachedImageThumbs;
	}
	
	public function setCacheInfo(array $cachedImageThumbs)
	{
		Cache::tags(config('imagable.cache_tag'))->forever(config('imagable.cache_key'), $cachedImageThumbs);
	}
	
	public function getCacheInfoKey($format)
	{
		$formatConfig = $this->getFormatConfig($format);
		
		return $formatConfig['width'].'x'.$formatConfig['height'];
	}
	
	public function getFormatConfig($format)
	{
		return config('imagable.formats')[$format];
	}
	
	public function getFormat($format)
	{
		if(!empty(config('imagable.formats')[$format])) {
			$cachedImageThumbs = $this->getCacheInfo();
			$formatConfig = $this->getFormatConfig($format);

			$cacheKey = $this->getCacheInfoKey($format);

			if(empty($cachedImageThumbs[$this->path][$cacheKey])) {
				return $this->generateFormat($format);
			}else{
				$cachedInfo = $cachedImageThumbs[$this->path][$cacheKey];
				
				if($cachedInfo == 1) {
					//thumb is available
					return Storage::url(config('imagable.thumbs_dir').'/'.$formatConfig['width'].'x'.$formatConfig['height'].'/'.trim($this->path, '/'));
				}else{
					return Storage::url($this->path);
				}
			}
		}else{
			throw new Exception('Image format not allowed. Please use: '.implode(', ', array_keys(config('imagable.formats'))));
		}
	}
	
	public function shouldGenerateFormat($format, $path = null)
	{
		$cachedImageThumbs = $this->getCacheInfo();
		$cacheKey = $this->getCacheInfoKey($format);
		
		return empty($cachedImageThumbs[$path ?: $this->path][$cacheKey]);
	}
	
	public function generateFormat($format, $path = null)
	{
		if(!empty(config('imagable.formats')[$format])) {
			$absolutePath = Storage::path($path ?: $this->path);
			list($originalWidth, $originalHeight) = getimagesize($absolutePath);
			
			$image = GlideImage::create($absolutePath);

			$formatConfig = $this->getFormatConfig($format);
			
			$cacheKey = $this->getCacheInfoKey($format);
		
			$deltaWidth = $originalWidth-$formatConfig['width'];
			$deltaHeight = $originalHeight-$formatConfig['height'];
					
			$cachedImageThumbs = $this->getCacheInfo();

			if($deltaWidth > 0 && $deltaHeight > 0) {
				if($deltaWidth > $deltaHeight) {
					//delta width largest, only shrink height
					$image = $image->modify(['h'=> $formatConfig['height'], 'q' => (!empty($formatConfig['quality']) ? $formatConfig['quality'] : config('imagable.default_quality'))]);
				}else{
					$image = $image->modify(['w'=> $formatConfig['width'], 'q' => (!empty($formatConfig['quality']) ? $formatConfig['quality'] : config('imagable.default_quality'))]);
				}

				$thumbPath = config('imagable.thumbs_dir').'/'.$formatConfig['width'].'x'.$formatConfig['height'].'/'.dirname($path ?: $this->path);

				Storage::makeDirectory($thumbPath, 0777);
				$image->save(Storage::path($thumbPath).'/'.basename($path ?: $this->path));

				$cacheInfo = 1;
			}else{
				$cacheInfo = 2;
			}
			$cachedImageThumbs[$path ?: $this->path][$cacheKey] = $cacheInfo;
			$this->setCacheInfo($cachedImageThumbs);

			if($cacheInfo == 1) {
				return Storage::url(config('imagable.thumbs_dir').'/'.$formatConfig['width'].'x'.$formatConfig['height'].'/'.($path ?: $this->path));
			}else{
				return Storage::url($path ?: $this->path);
			}
		}else{
			throw new Exception('Image format not allowed. Please use: '.implode(', ', array_keys(config('imagable.formats'))));
		}
	}
	
	public function generateThumbs($force = false)
	{
		if(config('imagable.auto_resize') == TRUE && is_array(config('imagable.default_formats'))) {
			if($force === TRUE) {
				$cachedImageThumbs = $this->getCacheInfo();
				if(is_array($this->paths)) {
					foreach($this->paths AS $path) {
						unset($cachedImageThumbs[$path]);
					}
				}else{
					unset($cachedImageThumbs[$this->path]);
				}
			
				$this->setCacheInfo($cachedImageThumbs);
			}
			
			foreach(config('imagable.default_formats') AS $format) {
				if(is_array($this->paths)) {
					foreach($this->paths AS $path) {
						if($this->shouldGenerateFormat($format, $path)) {
							$this->generateFormat($format, $path);
						}
					}
				}else{
					if($this->shouldGenerateFormat($format)) {
						$this->generateFormat($format);
					}
				}
			}
		}
	}
	
	public function flushCacheInfo()
	{
		return Cache::tags(config('imagable.cache_tag'))->flush();
	}
}
