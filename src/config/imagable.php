<?php

return [
	'auto_resize' => true,
	'default_quality' => 80,
	'cache_key' => 'cache_image_thumbs',
	'cache_tag' => 'image_thumbs',
	'formats' => [
		'xs' => [
			'width' => 120,
			'height' => 90,
			'quality' => 60,
		],
		'sm' => [
			'width' => 500,
			'height' => 400,
		],
		'md' => [
			'width' => 1000,
			'height' => 750,
		],
		'lg' => [
			'width' => 1600,
			'height' => 1200,
		],
		'xl' => [
			'width' => 2500,
			'height' => 1900,
		]
	],
	'default_formats' => [
		'xs', 'sm', 'md', 'lg'
	],
	'thumbs_dir' => '_thumbs'
];
